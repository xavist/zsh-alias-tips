autoload -Uz add-zsh-hook
autoload -U colors && colors

#Config
DF_ENABLE_ALIAS_SHOW=${DF_ENABLE_ALIAS_SHOW:-1}
DF_ENABLE_ALIAS_SHOW_EXPANDED=${DF_ENABLE_ALIAS_SHOW_EXPANDED:-1}
DF_ENABLE_ALIAS_TIPS=${DF_ENABLE_ALIAS_TIPS:-1}
DF_ENABLE_ALIAS_TIPS_ALL=${DF_ENABLE_ALIAS_TIPS_ALL:-0}

DF_ALIAS_TIPS_COLOR=${DF_ALIAS_TIPS_COLOR:-$fg[yellow]}

#Functions
_alias_show__preexec () {
  local -L orig="$1" #-L Disregard leading blanks when dealing with the value
  local cmd="$(alias ${orig%% *})" # Get command by removing everything from first space

  if [[ -n "$cmd" ]]; then
    if [[ "$DF_ENABLE_ALIAS_SHOW_EXPANDED" == 1 ]]; then
      echo "$cmd -> $3"
    else
      echo "$cmd"
    fi
  fi

  return 0
}

if [[ "$DF_ENABLE_ALIAS_SHOW" == 1 ]]; then
  add-zsh-hook preexec _alias_show__preexec
fi

_alias_tips__preexec () {
  local -L orig="$1" #-L Disregard leading blanks when dealing with the value

  alias | awk '{ print length($0) - index($0, "="), $0 }' | sort -nr | cut -f2- -d' ' | while read -r line; do
    if [[ "$orig" == "${(Q)line#*=}"* ]]; then
      echo "$DF_ALIAS_TIPS_COLOR$line$reset_color"
      [[ "$DF_ENABLE_ALIAS_TIPS_ALL" == 0 ]] && return 0
    fi
  done

  return 0
}

if [[ "$DF_ENABLE_ALIAS_TIPS" == 1 ]]; then
  add-zsh-hook preexec _alias_tips__preexec
fi
