# ZSH-ALIAS-TIPS - Show alias tips when executing commands

## Installing

Clone and source it in your *~/.zshrc* or use a plugin manager like [zgen](https://github.com/tarjoilija/zgen) or [antigen](https://github.com/zsh-users/antigen)

## Usage

It will automatically show you expanded alias and tips after each command execution. Check out *zsh-alias-tips.zsh* for some options you can configure
