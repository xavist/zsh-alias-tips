suite "Show"

local output

DF_ENABLE_ALIAS_SHOW=0
DF_ENABLE_ALIAS_TIPS=0

source ${0:A:h}/../zsh-alias-tips.zsh

function verify_alias_argument_and_result {
  output=$(_alias_show__preexec "$@")
  check_equal "$output" "$2"
}

zutest_no_show() {
  mock 'alias'

  verify_alias_argument_and_result 'ls' ''
}

zutest_show() {
  mock 'alias' 'echo $1'
  DF_ENABLE_ALIAS_SHOW_EXPANDED=0

  verify_alias_argument_and_result 'ls' 'ls'
  verify_alias_argument_and_result 'ls -G' 'ls'
  verify_alias_argument_and_result '  ls  ' 'ls'
  verify_alias_argument_and_result '  ls  -G  ' 'ls'
}

zutest_show_expanded() {
  mock 'alias' 'echo $1'
  DF_ENABLE_ALIAS_SHOW_EXPANDED=1

  verify_alias_argument_and_result 'ls' 'ls -> expanded' 'expanded'
  verify_alias_argument_and_result 'ls -G' 'ls -> expanded' 'expanded'
  verify_alias_argument_and_result '  ls  ' 'ls -> expanded' 'expanded'
  verify_alias_argument_and_result '  ls  -G  ' 'ls -> expanded' 'expanded'
}
